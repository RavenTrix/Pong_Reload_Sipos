using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;
public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    [HideInInspector] public bool pointChallenge;                               //se true la challenge scelta � a punti

    [HideInInspector] public float launchTimeRemaining = 4;                     //countdown per il lancio della pallina
    [HideInInspector] public int launchSeconds = 4;                             //countdown in secondi

    [HideInInspector] public int scoreP1, scoreP2;                              //punteggio dei due player

    [HideInInspector] public GameStatus gameStatus = GameStatus.gamePaused;     //lo status di gioco di default a paused

    [HideInInspector] public int seconds = 10;                                  //il timer della challenge a tempo in secondi
    public float timer = 10;                                                    //il timer della challenge a tempo

    public float maxPoint = 10;                                                 //massimo di punti per vincere
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("GameManager is null!");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }
    private void Start()
    {
        gameStatus = GameStatus.gamePaused;
    }

    public enum Player
    {
        player1,
        player2
    }

    public enum GameStatus
    {
        gameRunning,
        gamePaused
    }

    private void Update()
    {
        //se lo status di gioco � in paused il tempo si ferma, senn� riparte
        if (gameStatus == GameStatus.gamePaused)
        {
            Time.timeScale = 0;
        }
        else
            Time.timeScale = 1;

        //mi assicuro di essere nella scena di Pong per evitare missing references
        if (SceneManager.GetActiveScene().name != "PongScene") return;

        //faccio partire il countdown in secondi
        if (launchSeconds > 0)
        {
            launchTimeRemaining -= Time.deltaTime / 1.5f;
            launchSeconds = (int)(launchTimeRemaining % 60);
        }
        else
        {
            //prima di rilanciare la pallina mi assicuro che non si entri in gameOver allo scadere dei launchSeconds
            //cosa che pu� capitare se si fa goal all'ultimo secondo e questo fa perdere temporaneamente la reference
            //della pallina
            if (pointChallenge || (!pointChallenge && seconds != 0))
            {
                BallMovement ball = FindObjectOfType<BallMovement>();
                ball.Launch();
            }
        }

        //nella time challenge attivo il timer
        if (!pointChallenge)
        {
            UIManager.Instance.timerUI.SetActive(true);
            if (!(gameStatus == GameStatus.gameRunning)) return;
            if (!UIManager.Instance.countdownPanel.activeSelf && timer > 0)
                timer -= Time.deltaTime;
            seconds = (int)(timer % 60);

            UIManager.Instance.UpdateTimerUI(seconds);

            //quando il timer arriva a 0 finisce la partita
            if (seconds == 0)
            {
                GameOver();
            }
        }
        //nella point challenge disattivo il timer e controllo il punteggio per determinare il gameOver
        else
        {
            UIManager.Instance.timerUI.SetActive(false);
            timer = 0;

            if (scoreP1 == maxPoint || scoreP2 == maxPoint)
            {
                GameOver();
            }
        }

    }

    [Server] //� collegato al pulsante della time challenge e disattiva il menu della challenge
    public void TimeChallenge()
    {
        pointChallenge = false;
        UIManager.Instance.ServerChallengeMenu.SetActive(false);
        gameStatus = GameStatus.gameRunning;
    }
    [Server] //� collegato al pulsante della point challenge e disattiva il menu della challenge
    public void PointChallenge()
    {
        pointChallenge = true;
        UIManager.Instance.ServerChallengeMenu.SetActive(false);
        gameStatus = GameStatus.gameRunning;
    }

    //aumento il punteggio del player di 1 quando fa goal
    public void ScorePoint(Player player)
    {
        switch (player)
        {
            case Player.player1:
                scoreP1++;
                break;
            case Player.player2:
                scoreP2++;
                break;
            default:
                break;
        }
    }

    //in gameOver disattivo l'eventuale countDown e controllo i punteggi dei player per determinare il vincitore
    public void GameOver()
    {
        gameStatus = GameStatus.gamePaused;
        UIManager.Instance.countdownPanel.SetActive(false);
        string winner = "";
        if (scoreP1 > scoreP2)
        {
            winner = "Player 1";
        }
        else if (scoreP2 > scoreP1)
        {
            winner = "Player 2";
        }
        else
        {
            winner = "Nobody";
        }

        UIManager.Instance.ShowEndGameUI(winner);
    }

    //collegato al pulsante Back To Main Menu disattiva host e client e rimanda il player al main menu
    public void BackToMainMenu()
    {
        if (NetworkServer.active && NetworkClient.isConnected)
        {
            NetworkManager.singleton.StopHost();
        }
        else
        {
            NetworkManager.singleton.StopClient();
        }
        SceneManager.LoadScene("MenuScene");
    }

    //pulsante quit dei vari menu
    public void QuitGame()
    {
        Application.Quit();
    }
}
