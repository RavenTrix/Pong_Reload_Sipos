using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;

//l'host e il client hanno due challenge menu diversi:
//l'host ha due pulsanti di scelta (time challenge e point challenge) + quit
//il client non ha pulsanti
public class ChallengeUI : NetworkBehaviour
{
    public event Action<bool> ClientOnCloseChallengeMenu;                   //chiamato quando devo chiudere il menu delle challenge
    public event Action<bool> ClientOnDeactivateTimer;                      //chiamato quando disattivo il launch timer
    public event Action<bool> ClientOnPointChallenge;                       //chiamato quando scelgo la point challenge
    public event Action<GameManager.GameStatus> ClientOnChangeStatus;       //chiamato quando cambio status di gioco
    public static event Action ServerOnChooseChallenge;                     //chiamato quando il server deve scegliere la challenge
    public static event Action ClientOnChooseChallenge;                     //chiamato nel client quando il server deve scegliere la challenge

    [SyncVar]
    bool canClose;                  //se true si chiude la schermata di scelta challenge del client

    [SyncVar]
    bool canDeactivateTimer;        //se true si disattiva il launch timer

    [SyncVar]
    bool pointChallengeTrue;        //se true starta la point challenge

    [SyncVar]
    bool canChangeStatus;           //se true va in game running

    private void Update()
    {
        //quando il challenge menu sul server si disattiva
        if (!isClientOnly && !UIManager.Instance.ServerChallengeMenu.activeSelf)
        {
            CanCloseVoid(true);                             //do il permesso per chiudere il challenge menu sul client
            CanChangeVoid(true);                            //per settare lo status a game running
            if (!UIManager.Instance.timerUI.activeSelf)     //se il timer della modalit� a tempo � spento sul server
                CanDeactivateVoid(true);                    //lo spengo anche sul client
            if (GameManager.Instance.pointChallenge)        //se sul server viene scelta la point challenge
                PointChallenge(true);                       //anche sul client viene scelta la point challenge
        }

        //quando il client ha i permessi per chiudere il challenge menu
        if (isClientOnly && canClose == true)
        {
            ClientOnCloseChallengeMenu?.Invoke(false);      //lo disattiva

            if (canDeactivateTimer)                         //se ha finito il countdown pu� disattivare il timer
                ClientOnDeactivateTimer?.Invoke(false);

            if (canChangeStatus)                            //cambia status
                ClientOnChangeStatus?.Invoke(GameManager.GameStatus.gameRunning);

            if (pointChallengeTrue)                         //se sul server viene scelta la point challenge
                ClientOnPointChallenge?.Invoke(true);       //anche sul client parte la point challenge
            else
                ClientOnPointChallenge?.Invoke(false);      //senn� parte la time challenge
        }
    }

    #region Server
    public override void OnStartServer()
    {
        ServerOnChooseChallenge += ServerHandleChooseChallenge;

        if (!isClientOnly)
        {
            ServerOnChooseChallenge?.Invoke();
        }
    }

    public override void OnStopServer()
    {
        ServerOnChooseChallenge -= ServerHandleChooseChallenge;
    }

    [Server]  //disattivo il client-challenge-menu (quello che non ha pulsanti) sul server
    void ServerHandleChooseChallenge()
    {
        UIManager.Instance.ClientChallengeMenu.SetActive(false);
    }
    #endregion

    #region Client
    public override void OnStartClient()
    {
        ClientOnChooseChallenge += ClientHandleChooseChallenge;
        ClientOnCloseChallengeMenu += ClientHandleCloseChallengeMenu;
        ClientOnDeactivateTimer += ClientHandleDeactivateTimer;
        ClientOnChangeStatus += ClientHandleChangeStatus;
        ClientOnPointChallenge += ClientHandlePointChallenge;
        if (isClientOnly)
        {
            ClientOnChooseChallenge?.Invoke();
        }
    }

    public override void OnStopClient()
    {
        ClientOnChooseChallenge -= ClientHandleChooseChallenge;
        ClientOnCloseChallengeMenu -= ClientHandleCloseChallengeMenu;
        ClientOnDeactivateTimer -= ClientHandleDeactivateTimer;
        ClientOnChangeStatus -= ClientHandleChangeStatus;
        ClientOnPointChallenge -= ClientHandlePointChallenge;
    }

    [Client] //disattivo il server-challenge-menu (quello che ha la possibilit� di scelta) sul client
    void ClientHandleChooseChallenge()
    {
        UIManager.Instance.ServerChallengeMenu.SetActive(false);
    }

    [ClientRpc] //do il permesso di chiudere il client-challenge-menu sul client
    void CanCloseVoid(bool newCanClose)
    {
        canClose = newCanClose;
    }

    [Client] //disattivo il client-challenge-menu sul client dopo la scelta fatta dall'host
    public void ClientHandleCloseChallengeMenu(bool state)
    {
        UIManager.Instance.ClientChallengeMenu.SetActive(state);
    }
    
    [ClientRpc] //do il permesso di disattivare il il timer della modalit� a tempo
    void CanDeactivateVoid(bool newCanDeactivate)
    {
        canDeactivateTimer = newCanDeactivate;
    }

    [Client] //disattivo il timer della modalit� a tempo sul client
    public void ClientHandleDeactivateTimer(bool state)
    {
        UIManager.Instance.timerUI.SetActive(state);
    }

    [ClientRpc] //do il permesso di cambiare status di gioco
    void CanChangeVoid(bool newCanChange)
    {
        canChangeStatus = newCanChange;
    }

    [Client] //cambio status di gioco in game running sul client
    public void ClientHandleChangeStatus(GameManager.GameStatus state)
    {
        GameManager.Instance.gameStatus = state;
    }

    [ClientRpc] //controllo la challenge scelta dal server
    void PointChallenge(bool newPointCh)
    {
        pointChallengeTrue = newPointCh;
    }

    [Client] //setto la challenge scelta sul client
    public void ClientHandlePointChallenge(bool pointCh)
    {
        GameManager.Instance.pointChallenge = pointCh;
    }
    #endregion
}
