using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class LobbyMenu : MonoBehaviour
{
    [SerializeField] private GameObject lobbyUI;                            //il menu della lobby
    [SerializeField] private Button startGameButton;                        //il pulsante startGame della lobby
    [SerializeField] private TMP_Text[] playerNameTexts = new TMP_Text[2];  //i nomi dei giocatori
    [HideInInspector] public bool startButtonInteractable;                  //se true il player � l'host

    private void OnEnable()
    {
        PongNetworkManager.ClientOnConnected += HandleClientConnected;
        PongPlayer.ClientOnInfoUpdated += ClientHandleInfoUpdated;
    }
    private void OnDisable()
    {
        PongNetworkManager.ClientOnConnected -= HandleClientConnected;
        PongPlayer.ClientOnInfoUpdated -= ClientHandleInfoUpdated;
    }

    //quando il client si connette si apre il menu della lobby
    void HandleClientConnected()
    {
        lobbyUI.SetActive(true);
    }

    //se il client si disconnette, si disconnette solo lui
    //se a disconnettersi � l'host, il client perde la connessione al server
    //e torna al menu iniziale
    public void LeaveLobby()
    {
        if (NetworkServer.active && NetworkClient.isConnected)
        {
            NetworkManager.singleton.StopHost();
        }
        else
        {
            NetworkManager.singleton.StopClient();
            SceneManager.LoadScene(0);
        }
    }

    //collegato al pulsante start game interagibile solo dall'host che fa partire il gioco
    public void StartGame()
    {
        NetworkClient.connection.identity.GetComponent<PongPlayer>().CmdStartGame();
    }

    //ci sono due slot nomi, quando un player si connette vede il proprio nome apparire sul lobby menu
    //se il numero di giocatori connessi � minore al numero di slot nomi, appare la scritta "Waiting
    //for player..." sullo slot ancora vuoto
    //il pulsante di start � interagibile solo nel momento in cui ci sono 2 giocatori nella lobby e
    //solo dall'host
    private void ClientHandleInfoUpdated()
    {
        List<PongPlayer> players = ((PongNetworkManager)NetworkManager.singleton).Players;

        for (int i = 0; i < players.Count; i++)
        {
            playerNameTexts[i].text = players[i].PlayerName;
        }
        for (int i = players.Count; i < playerNameTexts.Length; i++)
        {
            playerNameTexts[i].text = "Waiting for player...";
        }

        if (players.Count > 1 && startButtonInteractable)
            startGameButton.interactable = true;
        else
            startGameButton.interactable = false;
    }

}
