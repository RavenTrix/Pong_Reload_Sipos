using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class UIManager : MonoBehaviour
{
    private static UIManager _instance;

    [SerializeField] public GameObject ClientChallengeMenu;     //il menu di challenge del client (senza possibilitÓ di scelta)
    [SerializeField] public GameObject ServerChallengeMenu;     //il menu di challenge dell'host
    [SerializeField] public GameObject timerUI;                 //il timer della modalitÓ a tempo
    [SerializeField] public GameObject countdownPanel;          //il panel che viene fuori al countdown
    [SerializeField] GameObject endGameCanvas;                  //il menu di gameOver
    [SerializeField] TMP_Text player1Score;                     //il testo dello score del player1
    [SerializeField] TMP_Text player2Score;                     //il testo dello score del player2
    [SerializeField] TMP_Text winnerText;                       //il testo che riporta il nome del vincitore
    [SerializeField] TMP_Text launchTimerText;                  //il testo del launch timer

    int p1, p2;     //i punteggi dei giocatori

    public static UIManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("GameManager is null!");
            }
            return _instance;
        }
    }
    private void Awake()
    {
        _instance = this;
    }
    private void Update()
    {
        p1 = GameManager.Instance.scoreP1;
        p2 = GameManager.Instance.scoreP2;

        //setto i testi dello score
        player1Score.text = "" + p1;
        player2Score.text = "" + p2;

        //setto il testo del launch timer
        launchTimerText.text = GameManager.Instance.launchSeconds.ToString();

        //disattivo il countdown panel quando il launch timer arriva a 0
        if (GameManager.Instance.launchSeconds == 0)
            countdownPanel.SetActive(false);
    }

    //aggiorno il timer della modalitÓ a tempo per il client
    public void UpdateTimerUI(float time)
    {
        timerUI.GetComponent<TMP_Text>().text = time.ToString();
    }

    //setto il nome del vincitore e attivo il menu di gameOver
    public void ShowEndGameUI(string winner)
    {
        endGameCanvas.SetActive(true);
        winnerText.text = winner + " wins!";
    }
}
