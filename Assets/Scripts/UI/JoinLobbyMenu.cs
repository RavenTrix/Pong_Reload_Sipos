using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Mirror;

public class JoinLobbyMenu : MonoBehaviour
{
    [SerializeField] private TMP_InputField addressInput;   //il network address al quale si connetter� il client
    [SerializeField] private Button joinButton;             //il pulsante per joinare il server


    //questo void � connesso al button di join e permette al client di connettersi al server tramite il networkAddress
    public void Join()
    {
        string address = addressInput.text;
        NetworkManager.singleton.networkAddress = address;
        NetworkManager.singleton.StartClient();
    }

    //questo void � connesso al button di host e permette al giocatore di fungere da host per la partita
    public void HostLobby()
    {
        NetworkManager.singleton.StartHost();
    }
}
