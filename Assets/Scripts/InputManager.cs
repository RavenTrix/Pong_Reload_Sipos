using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class InputManager : NetworkBehaviour
{
    [SerializeField] float speed = 12f;                     //la velocit� della paletta
    [SerializeField] public float boundaryHeight = 4f;      //la y dei boundaries

    Vector2 oldPos;                                         //la posizione precedente della paletta
    
    private void Update()
    {
        InputMovement();
        BoundariesCheck();
    }

    //controllo di avere autorit� sulla paletta e se dovesse trovarsi fuori da uno dei boundaries
    //tornerebbe alla posizione precedente
    void BoundariesCheck()
    {
        if (!isOwned) return;

        //il boundary in alto e offsettato di 1 a causa dell'area punteggi
        if (transform.position.y >= boundaryHeight - 1)
        {
            transform.position = new Vector2(oldPos.x, Mathf.RoundToInt(oldPos.y));
        }
        if (transform.position.y <= -boundaryHeight)
        {
            transform.position = new Vector2(oldPos.x, Mathf.RoundToInt(oldPos.y));
        }
        oldPos = transform.position;
    }

    //se ho autorit� sulla paletta, la sposto sull'asse verticale con W ed S o con le freccette su e gi�
    void InputMovement()
    {
        if (!isOwned) return;

        float vMove = Input.GetAxis("Vertical");

        transform.Translate(Vector2.up * Time.deltaTime * speed * vMove);

        //ho deciso di sfruttare il component NetworkTransform(Unreliable) per sincronizzare la posizione
        //delle palette da client a server

        //un'altra eventuale soluzione sarebbe stata la sincronizzazione con ClientRpc e Command che per� 
        //d� un lieve senso di lag back
    }
}
