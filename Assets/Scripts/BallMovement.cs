using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;

//la pallina ha un physic material privo di attrito e con una bounciness a 1.1 (in questo modo ad ogni rimbalzo accelera un po')
public class BallMovement : NetworkBehaviour
{
    [SerializeField] int force = 35;                    //forza con cui viene lanciata la pallina
    [HideInInspector] public bool alreadyLaunched;      //se true � stata lanciata

    public event Action OnClientP1ScoreUpdated;         //chiamato quando il player 1 fa punto
    public event Action OnClientP2ScoreUpdated;         //chiamato quando il player 2 fa punto
    public event Action OnClientBallOut;                //chiamato quando la pallina esce dai boundaries (quando rimbalza troppo)

    float multiplier = 10;                              //moltiplicatore della forza

    Rigidbody2D rb;
    InputManager IM;

    public override void OnStartClient()
    {
        OnClientP1ScoreUpdated += ClientHandleP1ScoreUpdated;
        OnClientP2ScoreUpdated += ClientHandleP2ScoreUpdated;
        OnClientBallOut += ClientHandleBallOut;
    }
    public override void OnStopClient()
    {
        OnClientP1ScoreUpdated -= ClientHandleP1ScoreUpdated;
        OnClientP2ScoreUpdated -= ClientHandleP2ScoreUpdated;
        OnClientBallOut -= ClientHandleBallOut;
    }
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        IM = FindObjectOfType<InputManager>();
    }
    private void Update()
    {
        //il server controlla se la pallina � andata oltre i boundaries e in quel caso resetta la sua posizione
        if (!isServer) return;

        //ho lasciato un piccolo offset sia sull'upper boundary che si trova ad altezza IM.boundaryHeight-1,
        //che al lower boundary che si trova ad altezza IM.boundaryHeight, per evitare che anche solo a toccarli
        //si resettasse la posizione della pallina
        if (transform.position.y > IM.boundaryHeight || transform.position.y < -IM.boundaryHeight - 2)
            OnClientBallOut?.Invoke();

        //ho deciso di sfruttare il component NetworkTransform(Unreliable) per sincronizzare la posizione
        //della pallina da server a client

        //un'altra eventuale soluzione sarebbe stata la sincronizzazione con ClientRpc e Command che per� 
        //d� un lieve senso di lag back
    }

    //resetto la posizione della pallina in modo che torni in centro
    public void ResetBall()
    {
        transform.position = new Vector2(0, 0);
        rb.velocity = Vector2.zero;
    }

    //se la pallina collide con il Goal del Player 1 d� il punto al Player 2 e viceversa
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("GoalP1"))
        {
            if (isServer)
                OnClientP2ScoreUpdated?.Invoke();
        }
        if (collision.transform.CompareTag("GoalP2"))
        {
            if (isServer)
                OnClientP1ScoreUpdated?.Invoke();
        }
    }

    //se la pallina non � ancora stata lanciata scelgo randomicamente una direzione in cui spedirla
    //e le do la forza per muoverla in quella direzione
    public void Launch()
    {
        if (alreadyLaunched) return;
        float horizontalDir = Mathf.Sign(UnityEngine.Random.Range(-100f, 100f));
        float verticalDir = UnityEngine.Random.Range(-1f, 1f);
        Vector2 dir = new Vector2(horizontalDir, verticalDir);
        rb.AddForce(dir * force * multiplier);
        alreadyLaunched = true;
    }

    [ClientRpc] //aggiorno il punteggio del player 1 sul client
    void ClientHandleP1ScoreUpdated()
    {
        GameManager.Instance.ScorePoint(GameManager.Player.player1);
        ResetBallAndTimer();
    }
    [ClientRpc] //aggiorno il punteggio del player 2 sul client
    void ClientHandleP2ScoreUpdated()
    {
        GameManager.Instance.ScorePoint(GameManager.Player.player2);
        ResetBallAndTimer();
    }
    [ClientRpc] //resetto la pallina quando esce
    void ClientHandleBallOut()
    {
        ResetBallAndTimer();
    }

    //resetto la posizione della pallina e il countdown
    void ResetBallAndTimer()
    {
        ResetBall();
        GameManager.Instance.launchSeconds = 4;
        GameManager.Instance.launchTimeRemaining = 4;
        alreadyLaunched = false;
        UIManager.Instance.countdownPanel.SetActive(true);
    }
}
