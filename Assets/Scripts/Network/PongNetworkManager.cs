using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Mirror;
using System;
public class PongNetworkManager : NetworkManager
{
    [SerializeField] GameObject playerPaddlePrefab;                     //la paletta
    [SerializeField] GameObject ballPrefab;                             //la pallina
    [SerializeField] GameObject challengeHandler;                       //il manager delle challenge
    public List<PongPlayer> Players { get; } = new List<PongPlayer>();  //i player in gioco

    public static event Action ClientOnConnected;                       //chiamato quando il client si connette

    private bool isGameInProgress = false;                              //se true la partita � gi� iniziata ed � al completo
    public bool IsGameInProgress { get { return isGameInProgress; } }

    #region Server
    //quando un client si connette al server, manda la richiesta di aggiunta del proprio player
    //e ne setta in automatico il suo nome
    public override void OnServerAddPlayer(NetworkConnectionToClient conn)
    {
        base.OnServerAddPlayer(conn);
        PongPlayer player = conn.identity.GetComponent<PongPlayer>();
        Players.Add(player);
        player.SetPlayerName($"Player {Players.Count}");
    }

    //quando avviene il cambio scena da quella offline a quella online, spawno il challengeHandler,
    //le due palette (una per player) e la pallina
    public override void OnServerSceneChanged(string sceneName)
    {
        if (SceneManager.GetActiveScene().name == "PongScene")
        {
            GameObject challengeHandlerInstance = Instantiate(challengeHandler);
            NetworkServer.Spawn(challengeHandlerInstance);
            foreach (PongPlayer player in Players)
            {
                GameObject playerPaddle = Instantiate(playerPaddlePrefab, GetStartPosition().position, Quaternion.identity);
                NetworkServer.Spawn(playerPaddle, player.connectionToClient);
            }
            GameObject ballInstance = Instantiate(ballPrefab);
            NetworkServer.Spawn(ballInstance);
        }
    }

    //se la partita � gi� iniziata e un client cerca di connettersi, il nuovo client viene disconnesso
    public override void OnServerConnect(NetworkConnectionToClient conn)
    {
        if (!isGameInProgress) return;
        conn.Disconnect();
    }

    //quando un client si disconnette viene rimosso il suo player
    public override void OnServerDisconnect(NetworkConnectionToClient conn)
    {
        PongPlayer player = conn.identity.GetComponent<PongPlayer>();
        Players.Remove(player);
        base.OnServerDisconnect(conn);
    }

    //quando viene stoppato il server, la lista di player si resetta ed � di nuovo possibile connettersi
    public override void OnStopServer()
    {
        Players.Clear();
        isGameInProgress = false;
    }

    //se e solo se ci sono due giocatori nella lobby, � possibile passare alla scena di gioco
    public void StartGame()
    {
        if (Players.Count != 2) return;
        isGameInProgress = true;
        ServerChangeScene("PongScene");
    }
    #endregion

    #region Client
    //quando il client si connette il menu della lobby si apre
    public override void OnClientConnect()
    {
        base.OnClientConnect();
        ClientOnConnected?.Invoke();
    }

    //quando viene stoppato un client, la lista dei giocatori viene resettata
    public override void OnStopClient()
    {
        Players.Clear();
    }
    #endregion

}
