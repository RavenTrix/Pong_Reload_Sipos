using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using Mirror;
public class PongPlayer : NetworkBehaviour
{
    [SyncVar(hook = nameof(ClientHandlePlayerNameUpdated))]
    private string playerName;                                  //sincronizzo il nome del player

    public string PlayerName { get { return playerName; } }

    public static event Action ClientOnInfoUpdated;             //viene chiamato quando viene settato il nome o cambiata un'informazione del client

    LobbyMenu lobbyM;

    private void Start()
    {
        lobbyM = FindObjectOfType<LobbyMenu>();

        //solo l'host ha la possibilitÓ di cliccare il pulsante "Start Game" della lobby
        if (isServer)
            lobbyM.startButtonInteractable = true;
        else
            lobbyM.startButtonInteractable = false;
    }

    #region Server
    //mi assicuro che il player non venga distrutto
    public override void OnStartServer()
    {
        DontDestroyOnLoad(gameObject);
    }

    [Command] //lancio il command per startare la partita
    public void CmdStartGame()
    {
        ((PongNetworkManager)NetworkManager.singleton).StartGame();
    }
    [Server] //setto il nome del player
    public void SetPlayerName(string newName)
    {
        playerName = newName;
    }
    #endregion


    #region Client
    //aggiungo il player alla lista players e mi assicuro che non venga distrutto
    public override void OnStartClient()
    {
        if (NetworkServer.active) return;
        ((PongNetworkManager)NetworkManager.singleton).Players.Add(this);

        DontDestroyOnLoad(gameObject);
    }

    //aggiorno le informazioni del Client, rimuovo il player e stoppo l'host per poter eventualmente
    //startare una nuova partita con qualcun altro
    public override void OnStopClient()
    {
        ClientOnInfoUpdated?.Invoke();
        if (!isClientOnly) return;
        ((PongNetworkManager)NetworkManager.singleton).Players.Remove(this);

        if (SceneManager.GetActiveScene().name == "PongScene")
        {
            NetworkManager.singleton.StopHost();
            SceneManager.LoadScene(0);
        }
    }

    //aggiorno il nome del player
    private void ClientHandlePlayerNameUpdated(string oldName, string newName)
    {
        ClientOnInfoUpdated?.Invoke();
    }
    #endregion
}
